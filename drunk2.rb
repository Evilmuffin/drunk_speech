class Command
  
  def initialize(input)
    @input = input
  end

  def input_check()
  command_check = @input.split(" ")
  
    while command_check[0] != "say" && command_check[0] != "set"
      invalid = [
        "Blah blah blah", 
        "My other car is a Lamborghini.",
        "I'm not listening to you anymore.",
        "If I want you to typo, I would've told you.",
        "Stop trying to type with yoour toes!",
        "Right!  And you are smart too!",
        "Maybe you should use a different keyboard...",
        "Hey!  I did a typo just like that!",
        "That was type #239098....Thanks for participating!",
        "You realiz that made absolutely no sense at all?"
        ].sample
      puts invalid
      print "> "
      input_again = $stdin.gets.chomp
      command_check = input_again.split(" ")
    end
  
  @input = command_check.join(' ')
  
  end
end

class Drunk < Command
  
  def initialize(input)
    super(input)
  end
  
  def check_command()
    if @input.split(" ")[0] == "say"
      return "do_say"
    else
      return "set_drunk"
    end
  end
  
  def drunk_speech()
    
    speech_input = @input.split(' ')
    speech_input.delete_at(0)
    speech_input = speech_input.join(' ')
    
    speech = speech_input.downcase.split('')
  
    speech.each do |content|
      
      content.sub!(/a/, ["A", "a", "a", "a", "AaaooU", "ouAA", "AAhH", "aaA",
                            "aAAaaaa", "AhHhh"].sample)
                            
      content.sub!(/b/, ["B", "b", "b", "b", "bBBbb", "BBbbB", "BbppbB",
                            "Bbb", "BBBb", "BpppPPp"].sample)
                        
      content.sub!(/c/, ["C", "c", "c", "c", "Ccc", "ccCc", "cc", "cCCC",
                            "ccc", "cCcCcc"].sample)
                        
      content.sub!(/d/, ["D", "d", "d", "d", "DdddD", "Dd", "dD", "ddd", "DD",
                          "DddDd"].sample)
                      
      content.sub!(/e/, ["E", "e", "e", "e", "e", "eH", "Eh", "ee", "eiiI",
                          "Eee"].sample)
                      
      content.sub!(/f/, ["F", "f", "f", "f", "ffPhh", "F", "F", "ff", "fF", 
                          "Ff"].sample)
                          
      content.sub!(/g/, ["G", "g", "g", "g", "G", "gg", "gG", "Gg",
                          "G"].sample)
                          
      content.sub!(/h/, ["H", "h", "h", "h", "H", "hh", "HH", "HhhH", 
                          "hHH"].sample)
                          
      content.sub!(/i/, ["I", "i", "i", "i"].sample)
      
      content.sub!(/j/, ["J", "j", "j", "Jj", "jJ"].sample)
      
      content.sub!(/k/, ["K", "k", "k", "k", "K", "KcA", "kCca"].sample)
      
      content.sub!(/l/, ["L", "l", "l"].sample)
      
      content.sub!(/m/, ["M", "m", "m", "mm", "Mm", "mM", "Mmmm", "mmMm",
                          "mmm", "mMMm"].sample)
                          
      content.sub!(/n/, ["N", "n", "n", "n", "nn", "N", "nn", "Nn", "nNn",
                          "NNn", "nNN"].sample)
                      
      content.sub!(/o/, ["O", "o", "o", "ooO", "OoO", "oo", "oO", "ooo", 
                          "oOoo", "OooO"].sample)
      content.sub!(/p/, ["P", "p", "p", "pPffftp"].sample)
      
      content.sub!(/q/, ["Q", "q", "q", "kuU", "qQ", "Quuq", "qkUu", "qq",
                          "Qq", "QQu"].sample)
                      
      content.sub!(/r/, ["R", "r", "r", "Rr", "rRR", "rr"].sample)
      
      content.sub!(/s/, ["sSssZZ", "Sss", "sSSzs", "sSsSs", "SZszzz", "Zzs", 
                          "zSzzs", "s", "s", "s"].sample)
                      
      content.sub!(/t/, ["T", "t", "t"].sample)
      
      content.sub!(/u/, ["U", "uhu", "UHu", "uuU", "Uuu", "UhhuHuhu", 
                          "uuuHHu", "u", "u"].sample)
                      
      content.sub!(/v/, ["V", "v", "v", "v"].sample)
      
      content.sub!(/w/, ["W", "w", "w"].sample)
      
      content.sub!(/x/, ["X", "iKsX", "XzX", "xxx", "xX", "x", "x", 
                          "x"].sample)
                          
      content.sub!(/y/, ["Y", "y", "y"].sample)
      
      content.sub!(/z/, ["Z", "z", "z", "Zzz", "sZzz", "ZzzZzz", "zZ", "ZSZ",
                          "Za"].sample)
                        
      if
        "0123456789".include?(content)
        content.sub!(/#{content}/, rand(9).to_s)
      end
    end
    drunk_output = speech.join
    puts "You say, '#{drunk_output}'"
    return "again"
  end
  
  def set_drunk(drunk_num)
    check_drunk = @input.split(' ')
    if check_drunk[1] != 'drunk'
      puts "The only think you can set is 'drunk'."
      return drunk_num
    elsif check_drunk[2].to_i < 1 || check_drunk[2].to_i > 10
      puts "You can only set drunk to between 1 - 10"
      return drunk_num
    else
      return check_drunk[2].to_i
    end
  end
  
  def normal_speech()
    normal_output = @input.split(' ')
    normal_output.delete_at(0)
    normal_output = normal_output.join(' ')
    puts "You say, '#{normal_output}'"
    return "again"
  end
  
end

setdrunk = 0
again = "again"

begin
  
  print "> "
  user_input = $stdin.gets.chomp.downcase
  
  if user_input == "quit"
    puts "Thanks for playing!  Good bye."
    exit(0)
  end
  
  while user_input == ""
    print "> "
    user_input = $stdin.gets.chomp.downcase
  end
  
  drunk_input = Drunk.new(user_input)
  drunk_input.input_check
  command = drunk_input.check_command()
  
  if command == "set_drunk"
    setdrunk = drunk_input.set_drunk(setdrunk)
  else
    if setdrunk < 5
      again = drunk_input.normal_speech()
    else
      again = drunk_input.drunk_speech()
    end
  end
  
end until again != "again"
