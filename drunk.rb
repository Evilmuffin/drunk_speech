#I will attempt to reproduce the drunk code in Strangemud

#Doing loop in case user want to play with the app again
begin
#First let's print a prompt to ask for a sentence.

  print "Please type a sentence: "
  user_input = gets.chomp
  
  #If someone enter nothing, let's prompt again.
  begin
    if user_input == ""
      puts "You didn't enter ANYTHING."
      print "Please type a sentence again: "
      user_input = gets.chomp 
    end
  end until user_input != ""
  
  # Turn the user_input into lower case then split the sentence into array.
  input = user_input.downcase.split('')
  
  # Check each content in the array and replace them accordingly.
  input.each do |content|
    
    content.sub!(/a/, ["A", "a", "a", "a", "AaaooU", "ouAA", "AAhH", "aaA",
                          "aAAaaaa", "AhHhh"].sample)
                          
    content.sub!(/b/, ["B", "b", "b", "b", "bBBbb", "BBbbB", "BbppbB",
                          "Bbb", "BBBb", "BpppPPp"].sample)
                      
    content.sub!(/c/, ["C", "c", "c", "c", "Ccc", "ccCc", "cc", "cCCC",
                          "ccc", "cCcCcc"].sample)
                      
    content.sub!(/d/, ["D", "d", "d", "d", "DdddD", "Dd", "dD", "ddd", "DD",
                        "DddDd"].sample)
                    
    content.sub!(/e/, ["E", "e", "e", "e", "e", "eH", "Eh", "ee", "eiiI",
                        "Eee"].sample)
                    
    content.sub!(/f/, ["F", "f", "f", "f", "ffPhh", "F", "F", "ff", "fF", 
                        "Ff"].sample)
                        
    content.sub!(/g/, ["G", "g", "g", "g", "G", "gg", "gG", "Gg",
                        "G"].sample)
                        
    content.sub!(/h/, ["H", "h", "h", "h", "H", "hh", "HH", "HhhH", 
                        "hHH"].sample)
                        
    content.sub!(/i/, ["I", "i", "i", "i"].sample)
    
    content.sub!(/j/, ["J", "j", "j", "Jj", "jJ"].sample)
    
    content.sub!(/k/, ["K", "k", "k", "k", "K", "KcA", "kCca"].sample)
    
    content.sub!(/l/, ["L", "l", "l"].sample)
    
    content.sub!(/m/, ["M", "m", "m", "mm", "Mm", "mM", "Mmmm", "mmMm",
                        "mmm", "mMMm"].sample)
                        
    content.sub!(/n/, ["N", "n", "n", "n", "nn", "N", "nn", "Nn", "nNn",
                        "NNn", "nNN"].sample)
                    
    content.sub!(/o/, ["O", "o", "o", "ooO", "OoO", "oo", "oO", "ooo", 
                        "oOoo", "OooO"].sample)
    content.sub!(/p/, ["P", "p", "p", "pPffftp"].sample)
    
    content.sub!(/q/, ["Q", "q", "q", "kuU", "qQ", "Quuq", "qkUu", "qq",
                        "Qq", "QQu"].sample)
                    
    content.sub!(/r/, ["R", "r", "r", "Rr", "rRR", "rr"].sample)
    
    content.sub!(/s/, ["sSssZZ", "Sss", "sSSzs", "sSsSs", "SZszzz", "Zzs", 
                        "zSzzs", "s", "s", "s"].sample)
                    
    content.sub!(/t/, ["T", "t", "t"].sample)
    
    content.sub!(/u/, ["U", "uhu", "UHu", "uuU", "Uuu", "UhhuHuhu", 
                        "uuuHHu", "u", "u"].sample)
                    
    content.sub!(/v/, ["V", "v", "v", "v"].sample)
    
    content.sub!(/w/, ["W", "w", "w"].sample)
    
    content.sub!(/x/, ["X", "iKsX", "XzX", "xxx", "xX", "x", "x", 
                        "x"].sample)
                        
    content.sub!(/y/, ["Y", "y", "y"].sample)
    
    content.sub!(/z/, ["Z", "z", "z", "Zzz", "sZzz", "ZzzZzz", "zZ", "ZSZ",
                        "Za"].sample)
                      
    if
      "0123456789".include?(content)
      content.sub!(/#{content}/, rand(9).to_s)
    end
  
  end

  output = input.join
  
  puts "I have no idea what you meant by,"
  puts "\"#{output}\"."
  puts "You must be drunk!\v"
  
  #Ask the user if he/she wants to do again.
  puts "Would you like to try again? Yes/No"
  print "> "
  cont = gets.chomp
  dcont = cont.downcase
  
  #Give funny reply if the user does not enter "yes" or "no".
  while dcont != "yes" and dcont != "no"
    typo = ["Blah blah blah", 
            "My other car is a Lamborghini.",
            "My other car is a Lamborghini.",
            "Do you understand the words that are coming out of my mouth?",
            "If I want you to typo, I would've told you.",
            "Which part of yes or no do you not understand?",
            "Right!  And you are smart too!",
            "Maybe you should use a different keyboard...",
            "Hey!  I did a typo just like that!",
            "That was type #239098....Thanks for participating!",
            "YES or NO"].sample
    puts typo
    print "> "
    cont = gets.chomp
    dcont = cont.downcase
  end 
 
end until dcont == "no" #End the loop if user does not want to try again.